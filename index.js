"use strict"

const p$is = require('@ptyps/is')
const https = require('https')
const http = require('http')
const url = require('url')

function request(to, data, opts) {
  return new Promise((res, rej) => {
    if (!opts && p$is.isObject(data))
      opts = data, data = null

    let uri = url.parse(to)

    if (!uri.protocol)
      throw new Error('Invalid URL provided, no protocol was specified.')

    let req, done = resp => {
      let recvd = '';

      resp.on('data', buffer => recvd += buffer)

      resp.on('end', () => res({
        status: resp.statusMessage,
        code: resp.statusCode,

        ... !resp.headers ? null : {
          headers: resp.headers
        },

        ... !recvd ? null : {
          body: recvd
        }
      }))
    }

    if (uri.protocol == 'https:')
      req = https.request(to, opts, done)

    if (uri.protocol == 'http:')
      req = http.request(to, opts, done)

    req.on('error', rej)

    req.end(data)   
  })
}